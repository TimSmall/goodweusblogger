#!/bin/bash

targetpath=/opt/goodweusblogger

id -u goodweusblogger &>/dev/null || sudo useradd --system goodweusblogger 

sudo ln -f -s ${targetpath}/systemd/system/goodweusblogger.service "/etc/systemd/system/goodweusblogger.service"

echo "Reload systemctl daemon"
sudo systemctl daemon-reload

echo "Start GoodWe USB Logger Service"
sudo systemctl enable goodweusblogger.service
sudo systemctl start goodweusblogger.service

sleep 15

sudo systemctl is-active --quiet goodweusblogger.service
if [ $? -eq 0 ]
then
    echo "GoodWe USB Logger Service started"
    exit 0
else
    echo "Service not started"
    exit 1
fi